// General
var bookTitle = 'Buku Kelas VIII IPA Semester 1';
var mode = 'twoPage';
var arrowControl = true;
var selectedPage;
var maxPage = 300;
var page = parseInt(getParameterByName('page'));
var pageRight = 0;
var pageLeft = 0;
var pinNavbar = false;

// DB
var stabilloDB = new PouchDB('stabilloDB');
var bookmarkDB = new PouchDB('bookmarkDB');
var remoteCouch = false;

// Stabillo
var stabilloMode = false;
var selectedText;
var stabilloChoice;

// Zoom
var zoomMode = false;

// Table of contents
var showTableOfContents = false;

// Start.
const $book = $('#book');
const $body = $('body');

const background = {
  light: '#ddd',
  dark: '#333',
  image: `url("${$('.background-image .img-thumbnail').attr('src')}")`,
};

/** 
 * Init Machine
 */
if (page <= 0 || page > maxPage) {
  $.notify('Halaman tidak ditemukan.','error');
  loadPage(1);
} else {
  loadPage(page);
  $('.input-page').val(page);
}

/**
 * Navbar
 */
let navHovered = false;
setTimeout(function(){
  if(!navHovered) {
    if (pinNavbar == false) {
      $('.navbar-hover').addClass('hiding');
      $('.tooltip-target').tooltip('disable');
    }
  }
}, 3000);
$('.navbar-hover').on('mouseover', function(){
  navHovered = true;
  if (pinNavbar == false) {
    $(this).removeClass('hiding');
    $('.tooltip-target').tooltip('enable');
  }
}).on('mouseout', function(){
  navHovered = false;
  if (pinNavbar == false) {
    $(this).addClass('hiding');
    $('.tooltip-target').tooltip('disable');
  }
})

$(document).ready(function () {

  /**
   * Enable Tooltip
   */
  $('[data-tooltip="tooltip"]').tooltip(
    { boundary: 'viewport', delay: { "show": 300, "hide": 10 }, template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>' }
  )

  // Show page corner fold when page is hovered and blurred
  $('.book__page').on('mouseover', function(){
    let side = $(this).data('side');
    $('.page-nav__'+side).addClass('open-small');
    $('.book__arrow-'+side).addClass('show');
    $('.book__arrow-'+side+'-onepage').addClass('show');
  })
  .on('mouseout', function(){
    let side = $(this).data('side');
    $('.page-nav__'+side).removeClass('open-small');
    $('.book__arrow-'+side).removeClass('show');
    $('.book__arrow-'+side+'-onepage').removeClass('show');
  })

  // Init background options
  let bgtype = localStorage.getItem('bgtype');
  if(bgtype == null) bgtype = 'light';
  $('input#background-'+bgtype).prop('checked', true);
  $('#background-image-list').toggle(bgtype === 'image');
  updateBackground(bgtype);

  // Init stabillo options
  stabilloChoice = localStorage.getItem('stabilloChoice');
  if(stabilloChoice == null) stabilloChoice = 'one';
  $('input#stabillo-'+stabilloChoice).prop('checked', true);

  // Init print options
  printNotes = localStorage.getItem('printNotes');

  if (printNotes == 'checked')
    $('#printNotes').prop('checked', true);
  else
    $('#printNotes').prop('checked', false);

  printStabillo = localStorage.getItem('printStabillo');

  if (printStabillo == 'checked')
    $('#printStabillo').prop('checked', true);
  else
    $('#printStabillo').prop('checked', false);
})
// END

/**
 * DOM Events
 */
$(document).ready(function () {
  
  /**
   * General
   */
  $(window).resize(function(){
    if ($(window).width() <= 1160) {
      $('input[name="layout"]')
        .prop({
          checked: false,
          disabled: true,
        })
        .trigger('change');
    } else {
      $('input[name="layout"]').prop({ disabled: false });
    }
  });

  $('.btn-pin-navbar').click(function(){
    if (pinNavbar == false) {
      pinNavbar = true;
      $(this).html('<i class="fas fa-toggle-on"></i>');
    } else {
      pinNavbar = false;
      $(this).html('<i class="fas fa-toggle-off"></i>');
    }
  });
  // END
  
  /**
   * Settings menu
   */
  $('[data-toggle="offcanvas"]').click(function (e) {
    e.preventDefault();

    const targetEl = $(this).data('target');
    $(targetEl).toggleClass('is-active');
  });

  $('input[name="layout"]').change(function () {
    if ($(this).prop("checked") == true){
      mode = 'onePage';
      $book.removeClass('book--two-page');
      $('.book__arrow-right-onepage').removeClass('sr-only');
      loadPage(getParameterByName('page'));
    } else {
      mode = 'twoPage';
      $book.addClass('book--two-page');
      $('.book__arrow-right-onepage').addClass('sr-only');
      location.reload();
      loadPage(getParameterByName('page'));
    }
    return false;
  });

  $('input[name="background"]').change(function () {
    const type = $('input[name="background"]:checked').val();

    $('#background-image-list').toggle(type === 'image');

    localStorage.setItem('bgtype', type);
    updateBackground(type);
  });

  $('input[name="stabillo"]').change(function () {
    stabilloChoice = $('input[name="stabillo"]:checked').val();
    localStorage.setItem('stabilloChoice', stabilloChoice);
    loadStabillo();
  });

  $('#printNotes').change(function () {
    if ($(this).is(":checked")) {
      localStorage.setItem('printNotes', 'checked');
    } else {
      localStorage.setItem('printNotes', '');
    }

    location.reload();
  });

  $('#printStabillo').change(function () {
    if ($(this).is(":checked")) {
      localStorage.setItem('printStabillo', 'checked');
    } else {
      localStorage.setItem('printStabillo', '');
    }
    
    location.reload();
  });

  $('.background-image__item').click(function (e) {
    e.preventDefault();

    $('.background-image__item.is-active').removeClass('is-active');
    $(this).addClass('is-active');

    localStorage.setItem('bgtype', 'image');
    localStorage.setItem('bgid', $(this).attr('id'));
    updateBackground('image');
  });
  // END

  /**
   * Navigation
   */
  $('.js-next-page').click((e) => {
    turnPageleft();

    return false;
  });

  $('.js-prev-page').click((e) => {
    turnPageRight();

    return false;
  });

  $(document).keydown(function(e) {
    if (arrowControl == true) {
      switch(e.which) {
          case 39: // Left
          turnPageleft();
          break;

          case 37: // Right
          turnPageRight();
          break;

          default: return;
      }
      e.preventDefault(); // Prevent the default action (scroll / move caret)
    }
  });

  $('.btn-close-sidebar').click(function(){
    showTableOfContents = false;
    $('.btn-show-table-of-contents').removeClass('active');
    $('.sidebar').hide();
  });
  // END

  /**
   * Jump to page.
   */
  $('.btn-jump').click(function(){
    arrowControl = false;
    $('#jumpModal').modal('show');

    return false;
  });

  $('.input-page').keypress(function (e) {
    if (e.which == 13) 
    {
      let inputPage = $(this).val();

      if (inputPage <= 0 || inputPage > maxPage) 
      {
        $.notify('Halaman tidak ditemukan.','error');
      } 
      else
      {
        arrowControl = true;
        page = inputPage;

        loadPage(page);
        
        $('#jumpModal').modal('hide');
        
        $('#flip-next').addClass('flip-next-enter-active').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
          $('#flip-next').removeClass('flip-next-enter-active');
        });
        
        new Audio('assets/sound/page-flip-01a.mp3').play();
      }
      
      return false;
    }
  });
  // END

  /**
   * Searching .. 
   */
  $('.btn-open-search').click(function(){
    arrowControl = false;
    $('#searchModal').modal('show');
  });

  $('.input-search').keypress(function (e) {
    if (e.which == 13) {
        search();
    }
  });
  
  $('.btn-search').click(function(){
    search();
  });

  $(document).on("click", ".btn-goto-search", function () {
    page = $(this).attr('data-page');
    loadPage(page);
    arrowControl = true;
    $('#searchModal').modal('hide');
    $('#flip-next')
      .addClass('flip-next-enter-active')
      .one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
        $('#flip-next').removeClass('flip-next-enter-active');
      });
    new Audio('assets/sound/page-flip-01a.mp3').play();

    return false;
  });
  // END

  /**
   * Bookmark
   */
  $('.btn-bookmark').click(function(){
    $('#bookmarkModal').modal('show');

    $('.bookmark-list').html('');

    bookmarkDB.allDocs({ include_docs: true, descending: true }, function (err, doc) {
        
        if (isEmpty(doc.rows)) {
          $('.bookmark-list').html('Belum ada ..');
        } else {
          $.each(doc.rows, function (index, value) {
              
              var output = `
                  <div class="item">
                    <div class="row">
                      <div class="col">
                        <a href="#" class="btn-table-contents" data-page="${value.doc.page}">Halaman ${value.doc.page}</a>
                      </div>
                      <div class="col">
                        <div class="text-right">
                          <a href="#" class="btn-remove-bookmark" data-rev="${value.doc._rev}" data-id="${value.doc._id}"><span class="far fa-trash-alt fa-fw"></span></a>
                        </div>
                      </div>
                    </div>
                  </div>
              `;

              $('.bookmark-list').append(output);
          });
        }
    });
  });
  
  $(document).on("click", ".btn-remove-bookmark", function () {
    var id = $(this).attr('data-id');
    var rev = $(this).attr('data-rev');
    
    bookmarkDB.remove(id, rev, null, function(){
        $('#bookmarkModal').modal('hide');
        $.notify('Berhasil dihapus.','success');
    });
  });

  $('.btn-save-bookmark').click(function () {
    
    if (mode == 'twoPage') 
    {
      let left = page;
      let right = left++;

      bookmarkDB.post({ page: left }, function callback(err, result) {
        if (!err) {
          bookmarkDB.post({ page: right }, function callback(err, result) {
            if (!err) {
                $.notify('Berhasil menyimpan halaman ' + right + ' dan ' + left,'success');
            } else {
                console.log('Failed to save ..');
            }
          });
        } else {
            console.log('Failed to save ..');
        }
      });
    } 
    else 
    {
      bookmarkDB.post({ page: page }, function callback(err, result) {
        if (!err) {
            $.notify('Berhasil menyimpan halaman ' + page,'success');
        } else {
            console.log('Failed to save ..');
        }
      });
    }
  });
  // END
  
  /**
   * Stabillo
   */
  $('.btn-stabillo').click(function(){
    
    // Hide context menu
    $('.context-menu').hide();

    if (stabilloMode == false) {
      stabilloMode = true;
      // Icons made by Freepik/Flaticon
      $('.book__content').css( 'cursor', 'url("assets/img/stabillo.png"), auto' );
      $.notify('Fitur penanda diaktifkan.','success');
      $('.btn-stabillo').addClass('active');  
    } else {
      stabilloMode = false;
      $('.book__content').css( 'cursor', 'default' );
      $.notify('Fitur penanda dimatikan.', 'info');
      $('.btn-stabillo').removeClass('active'); 
    }
  });

  $('.book__content__left').on("mouseup", function (event) {
      if (stabilloMode == true) {
        
        $('.btn-stabillo').addClass('active');
        
        if (window.getSelection) {
          selectedText = window.getSelection().toString();
        } else if (document.selection) {
          selectedText = document.selection.createRange().text;
        }
        
        if (selectedText != '') {
          
          stabilloDB.put({
            _id: new Date().toISOString(),
            page: pageLeft,
            selectedText: selectedText
          }, function callback(err, result) {
            if (!err) {
              loadStabillo();
            } else {
              console.log('Failed to save ..');
            }
          });
        }
      }

      return false;
  });

  $('.book__content__right').on("mouseup", function (event) {
      if (stabilloMode == true) {  
        
        $('.btn-stabillo').addClass('active');

        if (window.getSelection) {
          selectedText = window.getSelection().toString();
        } else if (document.selection) {
          selectedText = document.selection.createRange().text;
        }
        
        if (selectedText != '') {
          
          stabilloDB.put({
            _id: new Date().toISOString(),
            page: pageRight,
            selectedText: selectedText
          }, function callback(err, result) {
            if (!err) {
              loadStabillo();
            } else {
              console.log('Failed to save ..');
            }
          });
        }
      }

      return false;
  });

  $(document).on("click", ".btn-remove-selected", function (event) {
    removeStabillo(selectedPage);
    
    return false;
  });
  
  $(document).on("mousedown", ".book__content__right mark", function (event) {
    if (event.which == 3) {
      selectedPage = pageRight;
      $('.cm').hide();
      $('.cm-zoom, .cm-note, .cm-stabillo, .cm-share, .cm-remove-stabillo').show();
    }
  });
  $(document).on("mousedown", ".book__content__left mark", function (event) {
    if (event.which == 3) {
      selectedPage = pageLeft;
      $('.cm').hide();
      $('.cm-zoom, .cm-note, .cm-stabillo, .cm-share, .cm-remove-stabillo').show();
    }
  });
  // END

  /**
   * Home
   */
  $(document).on("click", ".btn-home", function () {
    window.location = "index.html";
    return false;
  });
  // END

  /**
   * Table of contents
   */
  $('.btn-show-table-of-contents').click(function(){
    if (showTableOfContents == false) {
      showTableOfContents = true;
      $('.btn-show-table-of-contents').addClass('active');
      $('.sidebar').show();
    } else {
      showTableOfContents = false;
      $('.btn-show-table-of-contents').removeClass('active');
      $('.sidebar').hide();
    }

    return false;
  }); 

  $(document).on('click', '.btn-table-contents', function () {
    showTableOfContents = false;
    
    // Hide everything
    $('.sidebar').hide();
    $('#bookmarkModal').modal('hide');
    
    // Trigger animation.
    $('#flip-next')
      .addClass('flip-next-enter-active')
      .one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
        $('#flip-next').removeClass('flip-next-enter-active');
      });
    
    // Sound
    new Audio('assets/sound/page-flip-01a.mp3').play();
    
    // Load page.
    page = $(this).attr('data-page');
    loadPage(page);
    
    return false;
  });
  // END

  /**
   * Zoom
   */
  $('.btn-zoom').click(function(){

    // Hide context menu
    $('.context-menu').hide();

    if (zoomMode == false) {
      $.notify('Fitur perbesar diaktifkan.','success');
      zoomMode = true;
      $(this).addClass('active');
      // Icons made by Freepik/Flaticon
      $('.book__content').css( 'cursor', 'url("assets/img/zoom.png"), auto' );
    } else {
      $.notify('Fitur perbesar dimatikan.','info');
      zoomMode = false;
      $(this).removeClass('active');
      $('.book__content').css( 'cursor', 'default' );
    }
  });

  document.querySelector( '.book__content__left' ).addEventListener( 'click', function( event ) {
    if (zoomMode == true) {
      event.preventDefault();
      zoom.to({ element: event.target });
    }
  });
  
  document.querySelector( '.book__content__right' ).addEventListener( 'click', function( event ) {
    if (zoomMode == true) {
      event.preventDefault();
      zoom.to({ element: event.target });
    }
  });
  // END

});

/**
 * Helper
 */
function updateBackground(type) {
  let bgid = localStorage.getItem('bgid');
  if(bgid === null) bgid = 'bg-1';

  $('.background-image__item').removeClass('is-active');
  $('#'+bgid).addClass('is-active');

  background.image = `url("${$('.background-image__item.is-active .img-thumbnail').attr('src')}")`;

  // Set attribution
  if($('#'+bgid).data('attribution') == ''){
    $('.background-attribution').addClass('sr-only');
  } else {
    $('.background-attribution').removeClass('sr-only');
    $('.background-attribution a span').html($('#'+bgid).data('attribution'));
    $('.background-attribution a').attr('href', $('#'+bgid).data('attribution-url'));
  }

  if (type == 'dark') {
    $('.nav').addClass('dark');
    $('.nav-link').addClass('dark');
    $('.book__page').addClass('dark');
  } else {
    $('.nav').removeClass('dark');
    $('.nav-link').removeClass('dark');
    $('.book__page').removeClass('dark');
  }

  if (type == 'light') {
    $body.css({
      background: '#ffefba',
      background: '-webkit-linear-gradient(to right, #ffefba, #ffffff)',
      background: 'linear-gradient(to right, #ffefba, #ffffff)',
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',
      backgroundPosition: '50% 80%',
    });
    $('.book__arrow a').addClass('text-dark').removeClass('text-light');
    $('.bg-overlay').addClass('sr-only');
  } else if (type == 'dark') {
    $body.css({
      background: '#141e30',
      background: '-webkit-linear-gradient(to right, #141e30, #243b55)',
      background: 'linear-gradient(to right, #141e30, #243b55)',
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',
      backgroundPosition: '50% 80%',
    });
    $('.book__arrow a').addClass('text-light').removeClass('text-dark');
    $('.bg-overlay').addClass('sr-only');
  } else {
    $body.css({
      background: background[type],
      backgroundSize: 'cover',
      backgroundAttachment: 'fixed',
      backgroundPosition: '50% 80%',
    });
    $('.bg-overlay').removeClass('sr-only');
    $('.book__arrow a').addClass('text-light').removeClass('text-dark');
  }
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'), results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function loadPage(page) {

  // Prepare
  $('.book__content__left').html('');
  $('.book__content__right').html('');
  $('.input-page').val(page);

  // Push to URL
  history.pushState(null, null, "?page=" + page);
  
  // TwoPage.
  if (mode == 'twoPage') 
  {
    pageLeft = page;
    pageRight = parseInt(pageLeft) + 1;
    
    $.ajax({
      method: "GET",
      url: 'content/' + pageLeft + '.html'
    })
    .fail(function () {
      $.notify('Halaman tidak ditemukan.','error');
    })
    .done(function (data) {
      $('.book__content__left').hide().html(data).fadeIn('slow');
      $('.loading').hide();
      
      // Lanjut load halaman kanan.
      $.ajax({
        method: "GET",
        url: 'content/' + pageRight + '.html'
      })
      .fail(function () {
        $.notify('Halaman tidak ditemukan.','error');
      })
      .done(function (data) {
        $('.book__content__right').hide().html(data).fadeIn('slow');
        $('.page-right').html(pageRight);
        $('.page-left').html(pageLeft);
        
        loadNotes('.book__content__left', pageLeft);
        loadNotes('.book__content__right', pageRight);
        loadStabillo();

        // Hide page button for page 1 and last page
        $('.page-nav__left').removeClass('sr-only');
        $('.book__arrow-left').removeClass('sr-only');
        $('.book__page--left').removeClass('first-page');
        if(pageLeft == 1){
          $('.page-nav__left').addClass('sr-only');
          $('.book__arrow-left').addClass('sr-only');
          $('.book__page--left').addClass('first-page');
        }

        $('.page-nav__right').removeClass('sr-only');
        $('.book__arrow-right').removeClass('sr-only');
        $('.book__page--right').removeClass('last-page');
        if(pageRight == maxPage){
          $('.page-nav__right').addClass('sr-only');
          $('.book__arrow-right').addClass('sr-only');
          $('.book__page--right').addClass('last-page');
        }

      });
    });
  }

  // OnePage.
  else if (mode == 'onePage') 
  {
    pageRight = page;

    $.ajax({
      method: "GET",
      url: 'content/' + pageRight + '.html'
    })
    .fail(function () {
      $.notify('Halaman tidak ditemukan.','error');
    })
    .done(function (data) {
      $('.book__content__right').hide().html(data).fadeIn('slow');
      $('.loading').hide();
      $('.page-right').html(pageRight);

      loadNotes('.book__content__right', pageRight);
      loadStabillo();
    });
  }
}

function search() {
  var input = $('.input-search').val();

  $.ajax({
    method: "GET",
    url: 'content.json'
  })
  .fail(function () {
    $.notify('Pencarian gagal.','error');
  })
  .done(function (data) {

    $('.search-list').html('');
    
    $.each(data, function (key, value) {
      let myReg = new RegExp(input + ".*");
      let myMatch = value.content.match(myReg);
      if (myMatch != null) {
        var output = `
          <a class="btn-goto-search" href="#" data-page="${value.page}">
            <div class="item">
              Hal ${value.page}: ${value.content}
            </div>
          </a>`;

        $('.search-list').append(output);
      }
    });
  });
}

function isEmpty(obj) {
  for (var key in obj) {
      if (obj.hasOwnProperty(key))
          return false;
  }
  return true;
}

function ObjectLength( object ) {
  var length = 0;
  for( var key in object ) {
      if( object.hasOwnProperty(key) ) {
          ++length;
      }
  }
  return length;
};

function loadStabillo() {    
  stabilloDB.allDocs({ include_docs: true, descending: true }, function (err, doc) {
    $.each(doc.rows, function (index, value) {
      if (value.doc.page == pageLeft || value.doc.page == pageRight) 
      {
        $('.book__content__left p, .book__content__right p').mark(value.doc.selectedText, {
          "accuracy": "complementary",
          "caseSensitive": true,
          "ignoreJoiners": true,
          "acrossElements": true,
          "separateWordSearch": false,
          "diacritics": false
        });

        if (localStorage.getItem('stabilloChoice') == 'one') {
          $('mark').css({
            'padding':'0px',
            'color': '#444',
            'background': '#2ecc71',
            'text-decoration': 'none',
            'position': 'relative'
          });
        } else if (localStorage.getItem('stabilloChoice') == 'two') {
          $('mark').css({
            'padding':'0px',
            'color': '#444',
            'background': 'orange',
            'text-decoration': 'none',
            'position': 'relative'
          });
        } else {
          $('mark').css({
            'padding':'0px',
            'color': '#444',
            'background': 'transparent',
            'text-decoration': 'underline',
            'position': 'relative'
          });
        }
      }
    });
    
  });

  return true;        
}

function removeStabillo(page)
{
  stabilloDB.allDocs({ include_docs: true, descending: true }, function (err, doc) {
    let length = ObjectLength(doc.rows);
    
    if (length > 0) {
      let i = 1;
      $.each(doc.rows, function (index, value) {
        if (value.doc.page == page) {
          stabilloDB.remove(value.doc._id, value.doc._rev, null);
        }
        
        if (i >= length) {
          $('.context-menu').hide();
          $('.cm-stabillo').show();
          $('.cm-remove-stabillo').hide();
          loadPage(parseInt(getParameterByName('page')));

          return false;
        }
        
        i++;
      });  
    }
  });

  return false;
}

function turnPageleft()
{
  let allowTurn = false;

  if (mode == 'twoPage') {
    if (page >= (maxPage - 1)) { 
      allowTurn = false;
    } else {
      allowTurn = true;
    }
  } else {
    if (page > maxPage) {
      allowTurn = false;
    } else {
      allowTurn = true;
    }
  }

  if (allowTurn == false) {
    $.notify('Halaman telah mencapai batas akhir.','error');
    return false;
  } else {
    new Audio('assets/sound/page-flip-01a.mp3').play();
    
    $('#flip-next').addClass('flip-next-enter-active').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
      $('#flip-next').removeClass('flip-next-enter-active');
    }); 

    if (mode == 'twoPage') {
      page++;
      page++;
    } else {
      page++;
    }

    loadPage(page);  
  } 
}

function turnPageRight()
{
  let allowTurn = false;

  if (mode == 'twoPage') {
    if (page <= 2) { 
      allowTurn = false;
    } else {
      allowTurn = true;
    }
  } else {
    if (page <= 1) {
      allowTurn = false;
    } else {
      allowTurn = true;
    }
  }

  if (allowTurn == false) {
    $.notify('Halaman telah mencapai batas awal.','error');
    return false;
  } else {
    new Audio('assets/sound/page-flip-01a.mp3').play();

    $('#flip-prev').addClass('flip-prev-enter-active').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
      $('#flip-prev').removeClass('flip-prev-enter-active');
    });

    if (mode == 'twoPage') {
      page--;
      page--;
    } else {
      page--;
    }

    loadPage(page);
  }
}